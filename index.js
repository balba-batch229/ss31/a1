/*

1. What built-in JS keyword is used to import packages?
-  require()
2. What Node.js module/package contains a method for server creation?
-  http
3. What is the method of the http object responsible for creating a server using Node.js?
-  createServer()
4. Between server and client, which creates a request?
-  request
5. Between server and client, which creates a response?
-  response
6. What is a runtime environment used to create stand-alone backend applications written in Javascript?
-  Node. js
7.What is the largest registry for Node packages?
-  NPM
*/

